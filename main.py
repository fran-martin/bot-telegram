#!/usr/bin/env python3

"""
Bot para Telegram
"""

# Importamos la libreria de python-telegram-bot
from telegram.ext import Updater, CommandHandler, MessageHandler, Filters
# Importamos la configuración
from config import TOKEN


def start(update, context):
    """ Envía un mensaje cuando recibe el comando /start. """
    # Obtener nombre del usuario
    # print(update.message)
    nombre_usuario = update.message.from_user.first_name
    update.message.reply_text(f'¡Hola! {nombre_usuario}, ¿Qué desea hacer?')

def help(update, context):
    """ Envía un mensaje cuando recibe el comando /help. """
    update.message.reply_text('¡Ayuda!')

def desconocido(update, context):
    """ Respuesta para los comandos desconocidos. """
    update.message.reply_text('Lo siento, no entendí ese comando.')


def main():
    # Creamos el updater y le pasmos el token del bot
    updater = Updater(token = TOKEN, use_context = True)
    dp = updater.dispatcher

    # Responder a los comandos
    dp.add_handler(CommandHandler('start', start))
    dp.add_handler(CommandHandler('help', help))

    # Responder a los comandos desconocidos
    dp.add_handler(MessageHandler(Filters.command, desconocido))

    
    # Iniciar el Bot
    updater.start_polling()
    print('El bot se está ejecutando...\n'\
        'Presione Ctrl-C para deternerlo.')

    # Ejecuta el bot hasta que presione Ctrl-C
    updater.idle()


if __name__ == '__main__':
    main()